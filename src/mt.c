#include "MT/mt.h"

#define    MT_BIT_WIDTH                          32

#define    MT_MULTIPLIER_OLD                  69069
#define    MT_DEFAULT_SEED32_OLD               4357
#define    MT_DEFAULT_SEED32_NEW               5489UL

#define    MT_KNUTH_MULTIPLIER_NEW       1812433253UL
#define    MT_KNUTH_SHIFT                        30

#define    MT_DEV_RANDOM           "/dev/random"
#define    MT_DEV_URANDOM          "/dev/urandom"

double mt_32_to_double;
double mt_64_to_double;

static uint32_t MT_devseed(MTState *state, const char* seed_dev);


MT_API void MT_mark_initialized(MTState *state) {
    mt_32_to_double = 1.0;
    
    for ( int32_t i = 0; i < MT_BIT_WIDTH; ++ i ) {
        mt_32_to_double /= 2.0;
    }
    mt_64_to_double = mt_32_to_double;
    for ( int32_t i = 0; i < MT_BIT_WIDTH; ++ i ) {
        mt_64_to_double /= 2.0;
    }
    state->initialized = true;
}

MT_API void MT_set_seed32(MTState *state, uint32_t seed) {
    if ( seed == 0 ) {
        seed = MT_DEFAULT_SEED32_OLD;
    }

    state->v[MT_STATE_SIZE - 1] = seed & 0xffffffff;
    for ( int i = MT_STATE_SIZE - 2; i >= 0; -- i ) {
        state->v[i] = ( MT_MULTIPLIER_OLD * state->v[i + 1] ) & 0xffffffff;
    }
    state->index = MT_STATE_SIZE;
    MT_mark_initialized( state );

    MT_refresh( state );
}

MT_API void MT_set_seed32new(MTState *state, uint32_t seed) {
    uint32_t next;

    state->v[MT_STATE_SIZE - 1] = seed & 0xffffffffUL;
    for ( int i = MT_STATE_SIZE - 2; i >= 0; -- i ) {
        next  = state->v[i + 1] >> MT_KNUTH_SHIFT;
        next ^= state->v[i + 1];
        next *= MT_KNUTH_MULTIPLIER_NEW;
        next += (MT_STATE_SIZE - 1) - i;

        state->v[i] = next & 0xffffffffUL;
    }
    state->index = MT_STATE_SIZE;
    MT_mark_initialized( state );

    MT_refresh( state );
}

MT_API void MT_seedfull(MTState *state, uint32_t seeds[MT_STATE_SIZE]) {
    _Bool    had_nz = false;

    for ( int i = 0; i < MT_STATE_SIZE; ++ i ) {
        if ( seeds[i] != 0 ) {
            had_nz = true;
        }
        state->v[MT_STATE_SIZE - i - 1] = seeds[i];
    }

    if ( !had_nz ) {
        abort();
    }
    state->index = MT_STATE_SIZE;
    MT_mark_initialized( state );
}

static uint32_t MT_devseed(MTState *state, const char* seed_dev) {
    FILE    *device_file;

    union {
        char      buffer[sizeof(uint32_t)];
        uint32_t  value;
    } random;

#ifdef WIN32
    struct _timeb    tb;
#else
    struct timeval   tv;
#endif  /* def WIN32 */

    device_file = fopen( seed_dev, "rb" );
    if ( device_file == NULL ) {
        device_file = fopen( MT_DEV_RANDOM, "rb" );
    }
    if ( device_file == NULL ) {
        setbuf( device_file, NULL );
    }

    int32_t  bytes_read  = 0;
    int32_t  next_byte   = 0;
    for ( next_byte = 0; next_byte < (int32_t)sizeof( random ); next_byte += bytes_read ) {
        bytes_read = fread( random.buffer + next_byte,
                            1,
                            sizeof(random.buffer) - next_byte, device_file );

        if ( bytes_read == 0 ) {
            break;
        }
    }
    fclose( device_file );

    if ( next_byte == sizeof(random.buffer) ) {
        MT_set_seed32new( state, random.value );
    }

#ifdef WIN32
    (void)_ftime( &tb );
#else
    (void)gettimeofday( &tv, NULL );
#endif   /* def WIN32 */

#ifdef WIN32
    random.value = tb.time   *    1000 + tb.millitm;
#else
    random.value = tv_tv_sec * 1000000 + tv.tv_usec;
#endif   /* def WIN32 */

    MT_set_seed32new( state, random.value );

    return random.value;
}

MT_API uint32_t MT_seed(MTState *state) {
    return MT_devseed( state, MT_DEV_URANDOM );
}

MT_API uint32_t MT_goodseed(MTState *state) {
    return MT_devseed( state, MT_DEV_RANDOM );
}

MT_API void MT_bestseed(MTState *state) {
    FILE *device_file = fopen( MT_DEV_RANDOM, "rb" );
    if ( device_file == NULL ) {
        MT_goodseed( state );

        return ;
    }

    int32_t  next_byte   = 0;
    int32_t  bytes_read   = 0;
    for ( next_byte = 0; next_byte < (int32_t)sizeof(state->v); next_byte += bytes_read ) {
        bytes_read = fread( state->v + next_byte,
                            1,
                            sizeof(state->v) - next_byte,
                            device_file );

        if ( bytes_read == 0 ) {
            fclose( device_file );

            MT_goodseed( state );

            return ;
        }
    }

    fclose( device_file );
}

void MT_refresh(MTState *state) {

}
