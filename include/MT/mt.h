#pragma once

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#ifdef WIN32
#   include <sys/timeb.h>
#endif  /* def WIN32 */

#define MT_API            extern

#define MT_STATE_SIZE     624

typedef struct mt_state_s {
    uint32_t    v[MT_STATE_SIZE];
    int32_t     index;
    _Bool       initialized;
    
} MTState;


/*!
 * 
 */
MT_API void         MT_mark_initialized(MTState *state);

/*!
 * 
 */
MT_API void         MT_set_seed32(MTState *state, uint32_t seed);

/*!
 * 
 */
MT_API void         MT_set_seed32new(MTState *state, uint32_t seed);

/*!
 * 
 */
MT_API void         MT_seedfull(MTState *state, uint32_t seeds[MT_STATE_SIZE]);

/*!
 * 
 */
MT_API uint32_t     MT_seed(MTState *state);

/*!
 * 
 */
MT_API uint32_t     MT_goodseed(MTState *state);

/*!
 * 
 */
MT_API void         MT_bestseed(MTState *state);
/*!
 * 
 */
MT_API void         MT_refresh(MTState *state);
